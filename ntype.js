"use strict";
exports.__esModule = true;
var ntype;
(function (ntype) {
    ntype["biography"] = "biography";
    ntype["historic"] = "historic";
    ntype["journal"] = "journal";
})(ntype = exports.ntype || (exports.ntype = {}));
;
