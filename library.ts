import {Author} from "./author";
import {Book} from "./book";
import {FictionBook} from "./FictionBook";
import {NonFictionBook} from "./NonFictionBook";
import {mtype} from "./mtype";
import {ntype} from "./ntype";

let book: Book[] = new Array();
book[0] = new Book("The name of the rose", 536,"Umberto", "Eco", true);
book[1] = new Book("Demian", 150, "Herman", "Hesse", true );
book[2] = new Book("Foundation", 550, "Isaac", "Asimov", true);
book[3] = new Book("The second sex", 850, "Simone", "De Beauvoir", false );
book[4] = new Book("Lord of the Rings: The two towers", 480, "J.R.R", "Tolkien", true);
book[5] = new Book("5 ways to make coffee, without coffee", 1000, "Despina", "Hohtoula", false , 24);
book[6] = new Book("The House of the Spirits", 300, "Isabel", "Allende", true);
book[7] = new Book("H Ellhnikh Antistash", 400, "Andreas", "Kedrou", false, 72);
book[8] = new Book("HTML & CSS", 600, "Jon", "Duckett", false, 40);
book[9] = new Book("Koinwnikh ekseliksh", 338, "Mixalis", "Dwrhs", false);

book[10] = new FictionBook("Cinderella", 20, "Charles", "Perrault",true,  mtype.ftale, 0 );
book[11] = new FictionBook(" Prelude to Foundation", 500, "Isaac", "Asimov",true, mtype.scfiction, 2);

book[12] = new NonFictionBook("H epidrash tou Nitse sthn Ellada", 240, "Kwstas", "Petropoulos", true, ntype.historic, 1);
book[13] = new NonFictionBook("The Diary of a Young Girl", 280, "Anne", "Frank", false, ntype.biography, 1);

book.forEach(element => {
    element.printBook();
});