"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var book_1 = require("./book");
var FictionBook = (function (_super) {
    __extends(FictionBook, _super);
    function FictionBook(title, pages, afname, alname, isFiction, type, dF, aAge) {
        var _this = _super.call(this, title, pages, afname, alname, isFiction, aAge) || this;
        _this.downloadFormat = ['pdf', 'epub', 'mobi'];
        _this.type = type;
        _this.downloadFormat[dF];
        return _this;
    }
    return FictionBook;
}(book_1.Book));
exports.FictionBook = FictionBook;
