"use strict";
exports.__esModule = true;
var author_1 = require("./author");
var Book = (function () {
    function Book(title, pages, authorFname, authorLname, isFiction, authorAge) {
        var _this = this;
        this.setTitle = function (newTitle) {
            _this.title = newTitle;
        };
        this.setAuthor = function (author) {
            _this.author = author;
        };
        this.setPages = function (pages) {
            _this.pages = pages;
        };
        this.setIsFiction = function (isFiction) {
            _this.isFiction = isFiction;
        };
        this.getAuthor = function () {
            return _this.author;
        };
        this.getTitle = function () {
            return _this.title;
        };
        this.getPages = function () {
            return _this.pages;
        };
        this.getIsFiction = function () {
            return _this.isFiction;
        };
        this.printBook = function () {
            console.log("Title: " + _this.title + " Pages: " + _this.pages + " Author: " + _this.author.printAuthor());
        };
        this.author = new author_1.Author(authorFname, authorLname, authorAge);
        this.title = title;
        this.pages = pages;
    }
    return Book;
}());
exports.Book = Book;
