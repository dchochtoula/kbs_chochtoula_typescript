import {AuthorInterface} from "./AuthInt"

export class Author implements AuthorInterface{
    first_name: string;
    last_name: string;
    age?: number;

    constructor(first_name: string, last_name: string, age?: number){
        this.first_name = first_name;
        this.last_name = last_name;
        this.age = age;
    }

    setFirst_name = (newfname: string) => {
        this.first_name = newfname;
    }

    setLast_name = (newlname: string) => {
        this.last_name = newlname;
    }

    setAge = (newAge: number) => {
        this.age = newAge;
    }

    getFirst_name = () => {
        return this.first_name;
    }

    getLast_name = () => {
        return this.last_name;
    }

    getAge = () => {
        return this.age;
    }

    printAuthor = () => {
        console.log("Firstname: "+this.first_name +", lastname: "+ this.last_name +", Age:"+ this.age);
    }
}