"use strict";
exports.__esModule = true;
var Author = (function () {
    function Author(first_name, last_name, age) {
        var _this = this;
        this.setFirst_name = function (newfname) {
            _this.first_name = newfname;
        };
        this.setLast_name = function (newlname) {
            _this.last_name = newlname;
        };
        this.setAge = function (newAge) {
            _this.age = newAge;
        };
        this.getFirst_name = function () {
            return _this.first_name;
        };
        this.getLast_name = function () {
            return _this.last_name;
        };
        this.getAge = function () {
            return _this.age;
        };
        this.printAuthor = function () {
            console.log("Firstname: " + _this.first_name + ", lastname: " + _this.last_name + ", Age:" + _this.age);
        };
        this.first_name = first_name;
        this.last_name = last_name;
        this.age = age;
    }
    return Author;
}());
exports.Author = Author;
