import {Book} from "./book"
import {ntype} from "./ntype"


export class NonFictionBook extends Book{
    type: ntype;    
    versions: any;

    constructor(title: string, pages: number, afname:string, alname: string, isFiction: boolean, type:ntype, versions: any ,aAge?: number){
        super(title, pages, afname, alname, isFiction, aAge);
        this.type = type;
        this.versions = versions;
    }
}