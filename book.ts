import { BookInterface } from "./BookInt"
import {Author} from "./author"
export class Book implements BookInterface{
    title: string;
    pages: number;
    author: Author;
    isFiction: boolean;


    constructor(title: string, pages: number, authorFname: string, authorLname: string , isFiction: boolean, authorAge ?: number){
        this.author = new Author(authorFname, authorLname, authorAge);
        this.title = title;
        this.pages = pages;


    }

    setTitle = (newTitle: string) => {
        this.title = newTitle;
    }

    setAuthor = (author: Author) => {
        this.author= author;
    }

    setPages = (pages: number) => {
        this.pages = pages;
    }

    setIsFiction = (isFiction: boolean) => {
        this.isFiction = isFiction;
    }

    getAuthor = () => { 
        return this.author
    }

    getTitle = () => {
        return this.title;
    }

    getPages = () => {
        return this.pages;
    }

    getIsFiction = () => {
        return this.isFiction;
    }

    printBook = () => {
        console.log("Title: "+this.title + " Pages: " + this.pages + " Author: " + this.author.printAuthor());
    }

}





