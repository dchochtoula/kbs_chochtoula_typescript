import {Author} from "./author"
export interface BookInterface{
    title: string;
    pages: number;
    author: Author;
    isFiction: boolean;

    setTitle(newTitle: string);
    setPages(newPages: number);
    setAuthor(newAuthor: Author);
    setIsFiction(fic: boolean);

    getTitle(): string;
    getPages(): number;
    getAuthor(): Author;
    getIsFiction(): boolean;

    printBook();
}