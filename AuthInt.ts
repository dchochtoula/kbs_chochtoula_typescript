export interface AuthorInterface{
    first_name: string;
    last_name: string;
    age?: number;

    setFirst_name(newfname: string);
    setLast_name(newlname: string);
    setAge(newAge: number);

    getFirst_name();
    getLast_name();
    getAge();

    printAuthor();
}