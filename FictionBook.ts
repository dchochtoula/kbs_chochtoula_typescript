import {Book} from "./book"
import {mtype} from "./mtype"

export class FictionBook extends Book{
    type: mtype;
    downloadFormat: string[] = ['pdf', 'epub', 'mobi'];

    constructor(title: string, pages: number, afname:string, alname: string, isFiction: boolean, type: mtype, dF: number, aAge?: number){
        super(title, pages, afname, alname, isFiction, aAge);
        this.type = type;
        this.downloadFormat[dF];

    }
}